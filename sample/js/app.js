import {Product} from './product.js';
let app=null;
let $$ = Dom7;
let kamelappLogo,userPhoto,islogin,popupProductAmount,productMightBuy
let previousSearch = '';
let sum = 0;
let buyProduct = [];
let isloginpage = false
//definition of app
window.onload=function () {
  let option = {
    id: 'app',
    el: '#app',
    ignoreCache: true,
    iosDynamicNavbar: false,
    view: {
        stackPages: true,
        allowDuplicateUrls: true,
        iosDynamicNavbar: false,
        iosSwipeBack:false,
    },
 
    touch: {
        materialRipple: false,
        tapHoldDelay: 500,
        tapHold: true,
    },
    lazy: {
        threshold: 50,
        sequential: true,
    },
    autoDarkTheme: false,
    material: true,
    theme: 'auto',
    materialRipple: false,
    allowDuplicateUrls: true,
    routes: routes
}
  app = new Framework7(option);
  //console.log(app)
  
  kamelappLogo= app.photoBrowser.create({
    photos: [{
      url: 'images/logoKamelapp.webp'
  }],
    navbar:false,
    toolbar:false
  });
  userPhoto = app.photoBrowser.create({
    photos: [{
      url: 'images/deafultPerson.jpeg'
  }],
    navbar:false,
    toolbar:false
} );
  
  if (localStorage.getItem('product')){
    openFirst()
  } else {
    console.log(2)
    test()
    alert('please refresh')
  }
}

function test(){
  console.log(localStorage)

  var category = ['لبنیات','نوشیدنی','تنقلات','بستنی','میوه','غذا','بهداشتی']
  localStorage.setItem('category',JSON.stringify(category))
  

  var product = [
    {
      'id': 1,
      'name':'شیر',
      'category':'لبنیات',
      'price':'15000',
      'discount':'5',
      'modulity':'وزن',
      'vahed':'لیتر',
      'amount':'188'
    },
    {
      'id': 2,
      'name':'های بای',
      'category':'تنقلات',
      'price':'10000',
      'discount':'0',
      'modulity':'تعداد',
      'vahed':'عدد',
      'amount':'166'
    },
    {
      'id': 3,
      'name':'کوکاکولا',
      'category':'نوشیدنی',
      'price':'9000',
      'discount':'0',
      'modulity':'تعداد',
      'vahed':'عدد',
      'amount':'246'
    },
    {
      'id': 4,
      'name':'کیم',
      'category':'بستنی',
      'price':'5000',
      'discount':'0',
      'modulity':'تعداد',
      'vahed':'عدد',
      'amount':'109'
    },
    {
      'id': 5,
      'name':'سالار',
      'category':'بستنی',
      'price':'20000',
      'discount':'5',
      'modulity':'تعداد',
      'vahed':'عدد',
      'amount':'132'
    },
    {
      'id': 6,
      'name':'سیب',
      'category':'میوه',
      'price':'30000',
      'discount':'7',
      'modulity':'وزن',
      'vahed':'کیلوگرم',
      'amount':'3'
    },
    {
      'id': 7,
      'name':'موز',
      'category':'میوه',
      'price':'35000',
      'discount':'11',
      'modulity':'وزن',
      'vahed':'کیلوگرم',
      'amount':'10'
    },
    {
      'id': 8,
      'name':'نارنگی',
      'category':'میوه',
      'price':'40000',
      'discount':'14',
      'modulity':'وزن',
      'vahed':'کیلوگرم',
      'amount':'170'
    },
    {
      'id': 9,
      'name':'پیتزا',
      'category':'غذا',
      'price':'100000',
      'discount':'20',
      'modulity':'تعداد',
      'vahed':'عدد',
      'amount':'180'
    },
    {
      'id': 10,
      'name':'ماست',
      'category':'لبنیات',
      'price':'60000',
      'discount':'15',
      'modulity':'وزن',
      'vahed':'لیتر',
      'amount':'1200'
    },
    {
      'id': 11,
      'name':'پنیر',
      'category':'لبنیات',
      'price':'14000',
      'discount':'15',
      'modulity':'وزن',
      'vahed':'لیتر',
      'amount':'10'
    },
  ]
  localStorage.setItem('product',JSON.stringify(product))
  
  
  var user = []
  localStorage.setItem('user',JSON.stringify(user))
  

  var seller = [
    {
      'id':1,
      'name':'ali',
      'password':'aliali',
      'darkmode':false,
      'maliat':false,
      'userInfoPage':false
    },
    {
      'id':2,
      'name':'mahdiyar',
      'password':'test',
      'darkmode':false,
      'maliat':false,
      'userInfoPage':false
    },
  ]
  localStorage.setItem('seller',JSON.stringify(seller))


  var boxbuy = []
  localStorage.setItem('boxbuy',JSON.stringify(boxbuy))

  var currentUser = '';
  localStorage.setItem('currentUser',JSON.stringify(currentUser))

  var currentSeller = '';
  localStorage.setItem('currentSeller',JSON.stringify(currentSeller))

  var currentBoxBuy = '';
  localStorage.setItem('currentBoxBuy',JSON.stringify(currentBoxBuy))
}

function openFirst(){
  kamelappLogo.open();
  let timeout = setTimeout(openSecond, 1000);
}

function openSecond(){
  kamelappLogo.close();
  if (JSON.parse(localStorage.getItem('currentSeller')) != ''){
    openNewPage('mainMenu',function(page){
      localStorage.setItem('currentBoxBuy',JSON.stringify(''))
      new mainMenuPage()
    })
  } else {
    openNewPage('login',function(page){
      islogin = true
      new loginPage();
    })
  }
}

// when you want to go new page
function openNewPage(page,callback) {
  console.log(page)
  app.views.main.router.navigate("/" + page + "/", null);
  setTimeout(function(){
    callback(page);
    pageDarkMode()
  },100)
}

function pageDarkMode(){
  let currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
  if(currentSeller['darkmode']){
    $$('div.page').addClass('dark');
  } else{
     $$('div.page').removeClass('dark');
  }
}

// complete close page
function closePage(page,callback) {
  
  app.views.main.router.back();
  setTimeout(function(){
    callback(page);
  },200)
}

function backToPage(url, callback) {
  app.views.main.router.back("/" + url + "/", {force: true, pushState: true});
  callback(url)
}

function loginPage(){
  var seller = JSON.parse(localStorage.getItem('seller'));
  
  $$('a.loginUser').on('click',function(e){
    $$('div.wrongPassword').text('');
    $$('div.wrongUsername').text('');
    var formData = app.form.convertToData('#loginForm');
    var username = formData['username'];
    var password = formData['password'];
    var isExist = false;
    
    for (let oneSeller of seller){
      if (oneSeller['id'] == username){
        isExist = true
        if(oneSeller['password'] == password){
          localStorage.setItem('currentSeller',JSON.stringify(oneSeller))
          
          openNewPage('mainMenu',function(page){
            
            let mainMenu = new mainMenuPage()  
          });
        } else {
          
          $$('div.wrongPassword').text('رمز اشتباه');
        }
      }
    }
    if (!isExist){
      $$('div.wrongUsername').text('کاربر یافت نشد');
    }
  });
}

function mainMenuPage(){
  
  $$('li.buySTH').on('click',function(e){
    $$('li.buySTH').transform('origin(10%)')
    function openpageproduct(){
      openNewPage('product',function(page){
        buyProduct = []
        new productPage();
      })
    }
    setTimeout(openpageproduct,200)
    
  });
  $$('li.boxBuy').on('click',function(e){
    function openpageboxbuy(){
      openNewPage('boxBuy',function(page){
        new boxBuyPage();
      });
    }
    setTimeout(openpageboxbuy,400)
    
  })
  $$('i.panelRrightOne').on('click',function(e){
    var panelRightOne = app.panel.create({
      content:`<div class="panel panel-right panel-cover panel-resizable panel-init">
      <div class="column">
        <div class="bg-color-blue no-margin-top ">
          <div class="item-inner item-cell justify-content-center">
            <div class="row justify-content-center">
              <div class="item-cell col-100"><img data-src="../images/logoKamelapp.webp" style="width: 100; height: 100px;"></div>
            </div>
            <div class="text-align-center">username</div>
          </div>
        </div>
        <div class="list">
          <ul class="column" >
            <li class="row justify-content-start margin-bottom-half margin-top-half">
              <a class="button padding-vertical col-100 justify-content-start" >
                <div class="item-content">
                  <div class="item-inner padding-right-half">
                    <div class="item-after"> 
                      <div><i class="bi bi-shop padding-top-half" style="font-size: 24px;"></i></div>
                      <b><div class="padding-right">مشخصات دستگاه</div></b>
                      <div>version: %%VERSION%%</div>
                    </div>
                  </div>
                </div>
              </a>
            </li>
  
            <li class="row justify-content-start margin-bottom-half margin-top-half">
              <a class="button padding-vertical col-100 justify-content-start" >
                <div class="item-content">
                  <div class="item-inner padding-right-half">
                    <div class="item-after"> 
                      <div><i class="bi bi-box" style="font-size: 24px;"></i></div>
                      <b><div class="padding-right">انبارداری </div></b>
                    </div>
                  </div>
                </div>
              </a>
            </li>
  
            <li class="row justify-content-start margin-bottom-half margin-top-half">
              <a class="button padding-vertical col-100 justify-content-start enterSetting" >
                <div class="item-content">
                  <div class="item-inner padding-right-half">
                    <div class="item-after"> 
                      <div><i class="bi bi-gear" style="font-size: 24px;"></i></div>
                      <b><div class="padding-right">تنظیمات</div></b>
                    </div>
                  </div>
                </div>
              </a>
            </li>
  
            <li class="row justify-content-start margin-bottom-half margin-top-half">
              <a class="button padding-vertical col-100 justify-content-start" >
                <div class="item-content">
                  <div class="item-inner padding-right-half">
                    <div class="item-after"> 
                      <div><i class="bi bi-download" style="font-size: 24px;"></i></div>
                      <b><div class="padding-right">بروزرسانی</div></b>
                    </div>
                  </div>
                </div>
              </a>
            </li>
  
            <li class="row justify-content-start margin-bottom-half margin-top-half">
              <a class="button padding-vertical col-100 justify-content-start exitButton" >
                <div class="item-content">
                  <div class="item-inner padding-right-half">
                    <div class="item-after"> 
                      <div><i class="bi bi-box-arrow-right" style="font-size: 24px;"></i></div>
                      <b><div class="padding-right">خروج</div></b>
                    </div>
                  </div>
                </div>
              </a>
            </li>
  
            <!-- another type of list
            <li class="justify-content-start margin-bottom-half margin-top-half padding-horizontal">
              <a class="row button col-100 justify-content-start">
                <div class="col-20 justify-content-start">
                  <i class="f7-icons">arrowshape_turn_up_right</i>
                </div>
                <div class="col-80 justify-content-start">
                  <b><div class="padding-right text-align-right">خروج</div></b>
                </div>
              </a>
            </li> -->
  
          </ul>
        </div>
        
        
      </div>
    </div>`,
    on: {
      closed: function () {
        app.panel.destroy('')
      }
    },
    resizable:false
  })
    panelRightOne.open()
    $$('a.exitButton').on('click',function(e){
      panelRightOne.close()
      
      localStorage.setItem('currentSeller',JSON.stringify(''))
      if (isloginpage == true){
        closePage('mainMenu',function(page){})
      } else {
        openNewPage('login',function(page){
          islogin = true
          new loginPage();
        })
      }
    });

    $$('a.enterSetting').on('click',function(e){
      panelRightOne.close()
      var settingPopUp = app.popup.create({
        content:`
            <div class='popup' >
              <div class="navbar no-margin-bottom" style="border-bottom: 1px solid rgb(196, 196, 196);">
                <div class="row padding-left">
                  <div class='col-40 margin-top-half'>
                    
                  </div>
                  <div class="col-50 padding-right padding-top-half text-align-right">
                    تنظیمات
                  </div>
                  <div class="col-10 margin-top-half padding-left-half">  
                    <i class="icon icon-back closeSetting"></i>
                  </div>
                </div>
              </div>
              <div class="row  justify-content-center" style="width: 100%;">
                <div class='col-80 row no-gap margin-vertical-half'>
                  <div class='col-70' style="font-size: 20px;">
                    رنگ زمینه
                  </div>
                  <div class='col-30'>
                    <input type="checkbox" class="checkbox toggleDarkMode bg-color-red" id="checkboxDarkmode" style="display: none;">
                    <label for="checkboxDarkmode" class="checkboxDarkmode toggleDarkMode bg-color-gray">
                      <i class="bi bi-moon-fill" style="color: gray;visibility:hidden; "></i>
                      <i class="bi bi-sun-fill" style="color: yellow; visibility:hidden;"></i>
                      <span class="ball text-align-center darkmodeBall">
                      <i class="bi bi-sun-fill margin-top padding-top" style="color: blue; font-size:20px; margin-top:20px;"></i>
                      </span>
                    </label>
                  </div>

                </div>
                <div class="col-80 row no-gap margin-vertical-half">
                  <div class="col-70" style="font-size: 20px;">
                    گرفتن مالیات 
                  </div>
                  <div class="col-30">
                    <input type="checkbox" class="checkbox toggleMaliat bg-color-red" id="checkboxMaliat" style="display: none;">
                    <label for="checkboxMaliat" class="checkboxMaliat toggleMaliat bg-color-red">
                      <div></div>
                      <div></div>
                      <span class="ball"></span>
                    </label>
                  </div>
                </div>
          
                <div class="col-80 row no-gap margin-vertical-half">
                  <div class="col-70" style="font-size: 20px;">
                    گرفتن اطلاعات مشتری 
                  </div>
                  <div class="col-30">
                    <input type="checkbox" class="checkbox toggleUserInfo bg-color-red" id="checkboxUserInfo" style="display: none;">
                    <label for="checkboxUserInfo" class="checkboxUserInfo toggleUserInfo bg-color-red">
                      <div></div>
                      <div></div>
                      <span class="ball bg-color-black"></span>
                    </label>
                  </div>
                </div>
                
                
                
               
              </div>
            </div>`,
        on:{
          closed: function (popup) {
          app.popup.destroy('')
          }
        }
      })
      settingPopUp.open()
      let currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
      if(currentSeller['darkmode']){
        $$('div.popup').addClass('dark')
      } else{
        $$('div.popup').removeClass('dark')
      }
      $$('i.closeSetting').on('click',function(e){
        settingPopUp.close()
      })
      
      $$('.toggleDarkMode').on('change',function(e){
        
        let seller = JSON.parse(localStorage.getItem('seller'))
        let currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
        if(currentSeller['darkmode'] == false){
          currentSeller['darkmode'] = true
        } else{
          currentSeller['darkmode'] = false
        }
        let i =0 ;
        for (let oneSeller of seller){
          if(oneSeller['id'] != currentSeller['id']){
            i =i+1;
            continue
          }
          seller[i] = currentSeller
        }
        localStorage.setItem('seller',JSON.stringify(seller))
        localStorage.setItem('currentSeller',JSON.stringify(currentSeller))
        
        if(currentSeller['darkmode']){
          $$('div.popup').addClass('dark')
        } else{
          $$('div.popup').removeClass('dark')
        }
        pageDarkMode()
        firstSet()
      })
      $$('.toggleMaliat').on('change',function(e){
        let seller = JSON.parse(localStorage.getItem('seller'))
        let currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
        if (currentSeller['maliat'] == true){
          currentSeller['maliat'] = false
        } else {
          currentSeller['maliat'] = true
        } 
        let i = 0;
        for (let oneSeller of seller){
          if(oneSeller['id'] != currentSeller['id']){      
            i = i+1
            continue
          }
          seller[i] = currentSeller
        }
        localStorage.setItem('seller',JSON.stringify(seller))
        localStorage.setItem('currentSeller',JSON.stringify(currentSeller))
        firstSet()
      })
    
      $$('.toggleUserInfo').on('change',function(e){
        let seller = JSON.parse(localStorage.getItem('seller'))
        let currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
        if (currentSeller['userInfoPage'] == true){
          currentSeller['userInfoPage'] = false
        } else {
          currentSeller['userInfoPage'] = true
        } 
        let i = 0;
        for (let oneSeller of seller){
          if(oneSeller['id'] != currentSeller['id']){      
            i = i+1
            continue
          }
          seller[i] = currentSeller
        }
        localStorage.setItem('seller',JSON.stringify(seller))
        localStorage.setItem('currentSeller',JSON.stringify(currentSeller))
        firstSet()
      })
      firstSet()
      function firstSet(){
        var currentSeller = JSON.parse(localStorage.getItem('currentSeller'))
        if (currentSeller['darkmode']){
          $$('input.toggleDarkMode').prop('checked',true)
          $$('.darkmodeBall').html(`<i class="bi bi-moon-fill margin-top padding-top" style="color: blue; font-size:20px; margin-top:20px;"></i>`)
          $$('label.checkboxDarkmode').removeClass('bg-color-green')
          $$('label.checkboxDarkmode').addClass('bg-color-gray')
        } else {
          $$('input.toggleDarkMode').prop('checked',false)
          $$('.darkmodeBall').html(`<i class="bi bi-sun-fill margin-top padding-top" style="color: blue; font-size:20px; margin-top:20px;"></i>`)
          $$('label.checkboxDarkmode').removeClass('bg-color-gray')
          $$('label.checkboxDarkmode').addClass('bg-color-green')
        }
    
        if (currentSeller['maliat']){
          $$('input.toggleMaliat').prop('checked',true)
          $$('label.toggleMaliat').removeClass('bg-color-green')
          $$('label.toggleMaliat').addClass('bg-color-gray')
        } else {
          $$('input.toggleMaliat').prop('checked',false)
          $$('label.toggleMaliat').removeClass('bg-color-gray')
          $$('label.toggleMaliat').addClass('bg-color-green')
        }
    
        if (currentSeller['userInfoPage']){
          $$('input.toggleUserInfo').prop('checked',true)
          $$('label.toggleUserInfo').removeClass('bg-color-green')
          $$('label.toggleUserInfo').addClass('bg-color-gray')
        } else {
          $$('input.toggleUserInfo').prop('checked',false)
          $$('label.toggleUserInfo').removeClass('bg-color-gray')
          $$('label.toggleUserInfo').addClass('bg-color-green')
        }
      }
    });
  })
}

function productPage(){
  let products = JSON.parse(localStorage.getItem('product'))
  function test(){
    fetch('https://api.nationalcurrencyapi.com/forex-rates')
    .then(response => response.json())
    .then(data => {
      const usdRate = data.rates.Usd;
      console.log('rdlj: ',+usdRate)
    })
    .catch(error => {
      console.error('khata'+error)
    })
  }
  test()
  updateSum();
  let allOfCategory = `<div class="padding-horizontal bg-color-customOne categorySelect margin-left-half" style='border-radius:10px; border:1px solid lightblue'>همه</div>`;
  let currentCategory = ''
  
  let categories = JSON.parse(localStorage.getItem('category'))
  
  
  // show category
  for (let oneCategory of categories){
    allOfCategory = allOfCategory + `<div class="padding-horizontal categorySelect margin-horizontal-half" style='border-radius:10px; border:1px solid lightblue'>${oneCategory}</div>`;
  }
  $$('div.categories').html(allOfCategory);
  showProduct('');

  function listenerOne(){
    $$('a.addProductToBox').on('click',function(e){
      let addProduct = $$(this).parent().children('div').text();
      for (let oneProduct of products){
        if (oneProduct['id'] != addProduct){
          continue;
        }
        let dialogProductAmount = app.dialog.create({
          content:`
                  <div class='text-align-center justify-content-center no-margin no-padding' style="font-size: 14px;">
                    <h4>${oneProduct['modulity']} محصول خود را به ${oneProduct['vahed']} وارد کنید</h4>
                    <input type="number" name="name" placeholder= ${oneProduct['vahed']} class=' justify-content-center amountProduct' style='border:1px solid gray; border-radius:2px; padding:5px; width:100%'/>
                   
                  `,
          closeByBackdropClick:true,
          buttons:[
            {
              text:'تایید',
              bold:true,
              color:'white',
              cssClass:'margin-left padding-horizontal bg-color-blue',
              onClick:function(dialog,e){
                if($$('input.amountProduct').val()){
                  let oneProduct ;
                  for ( oneProduct of products){
                    if (oneProduct['id'] != addProduct ){continue}
                    break
                  }
                  if (parseInt(oneProduct['amount']) < parseInt($$('input.amountProduct').val())){
                    
                    let toastIsNotEnough = app.dialog.create({
                      content:`
                        <div class='row text-align-center justify-content-center' style='font-size:18px;'>
                          <div class='col-80 text-align-center'>
                            در انبار نیست
                          </div>
                          
                        </div>
                      `,
                      buttons:[
                        {
                          text:`<div class='margin-left'><div class='margin-left'><div class='margin-left'><div class='margin-left'><div class='margin-left'><div class='margin-left'>ok</div></div></div></div></div></div>`,
                          cssClass:'margin-left paddding-left',
                          keyCodes:[13],
                          onClick:function(){
                            app.dialog.close()
                          }
                        }
                      ],
                      on:{
                        closed:function(){
                          app.dialog.destroy()
                        }
                      }
                      
                    })
                    toastIsNotEnough.open()
                    
                    
                  } else {
                    if($$('input.amountProduct').val() == 0){
                      let i =0 ;
                      for (let oneProductBuy of buyProduct){
                        if (oneProductBuy['id'] == addProduct){
                          buyProduct.splice(i,1);
                        }
                      }

                    } else {
                      let isexist =false ;
                      for (let oneProductBuy of buyProduct){
                        if (oneProductBuy['id'] == addProduct){
                          oneProductBuy['weight'] = $$('input.amountProduct').val();
                          isexist = true;
                        }
                      }
                      if (isexist == false){
                        buyProduct.push({'id':oneProduct['id'],'weight':$$('input.amountProduct').val()})
                      }
                                       
                      localStorage.setItem('product',JSON.stringify(products))
                    }
                  }
                }
                showProduct(previousSearch);
                dialog.close();
              }
            },
            {text:'انصراف',bold:true,color:'white',cssClass:'margin-horizontal padding-horizontal bg-color-red',onClick:function(dialog,e){dialog.close()}}
          ],
          on:{
            'closed':function(dialog){
            app.dialog.destroy();
            }
          }
        });
        dialogProductAmount.open();
        var seller = JSON.parse(localStorage.getItem('currentSeller'))
        if (seller['darkmode']){
          $$('div.dialog').addClass('dark')
        }
        updateSum();
        
        break;
      }  
      
    });

    $$('a.removeProductFromBox').on('click',function(e){
      let removeProduct = $$(this).parent().children('div').text();
      let i =0 ;
      for (let oneProductBuy of buyProduct){
        if (oneProductBuy['id'] == removeProduct){
         
          for (let oneProduct of products){
            if(oneProduct['id'] != removeProduct){continue}
          }
          localStorage.setItem('product',JSON.stringify(products))
          buyProduct.splice(i,1);
          showProduct(previousSearch);
          break;
        }
        i = i+1;
      }
      updateSum();
      
    });
  }

  $$('div.categorySelect').on('click',function(e){
    $$('div.categorySelect').removeClass('bg-color-customOne')
    currentCategory = $$(this).text();
    $$(this).addClass('bg-color-customOne')
    if (currentCategory == 'همه'){
      currentCategory = ''
    }
    showProduct($$('input.search').val())
  });

  $$('input.search').on('keyup',function(e){
      showProduct($$(this).val());
  });

  $$('i.barcode').on('click',function(e){
    let chooseCameraBarcodeActionsheet = app.sheet.create({
      content:`
              <div class="sheet-modal" style='border-radius:10px'>
                <div class='title text-align-center margin-vertical padding-bottom' style="font-size:20px; border-bottom: 1px solid;">انتخاب دستگاه</div>
                <div class='row'>
                  <div class='col-50  text-align-center'>
                    <a class='text-color-purple text-align-center margin-top'><i class="bi bi-camera text-align-center" style='font-size:55px'></i></a>
                  </div>
                  <div class='col-50  text-align-center'>
                    <a class='text-color-purple text-align-center margin-top'><i class="bi bi-upc text-align-center" style='font-size:55px'></i></a>
                  </div>
                </div>
              </div>`,
      on:{
          'closed':function(dialog){
          app.sheet.destroy();
        }
      }
    });
    chooseCameraBarcodeActionsheet.open();
    var seller = JSON.parse(localStorage.getItem('currentSeller'))
        if (seller['darkmode']){
          $$('div.sheet-modal').addClass('dark')
        }
  });

  $$('a.userInfoPage').on('click',function(e){
    if (sum > 0){
      let checkInfoPage = JSON.parse(localStorage.getItem('currentSeller'))
      if (checkInfoPage['userInfoPage']){
        
        var userInfoPage = app.popup.create(
          {
            content:`
            <div class='popup'>
              <div data-page="userInfo" class="page">
                <div class="navbar "style="border-bottom: 1px solid rgb(196, 196, 196);">
                  <div class="row">
                    <div class="col-50 padding-right padding-top-half">
                      اطلاعات مشتری
                    </div>
                    <div class="col-15 margin-top">
                      <i class="icon icon-back backToProductPage"></i>
                    </div>
                  </div>
                </div>
                <div class="toolbar toolbar-bottom margin-right">
                  <div class="toolbar-inner margin-right">
                    <div class="padding-right">
                      <p class="sumPrice"></p>
                      
                    </div>
                    <a href="" class="button button-fill margin-left boxBuyCustomerPage bg-color-green" style=font-size:18px;><b>مرحله بعد</b></a>
                  </div>
                </div>
                <div class="page-content">
                  
                  <div class="block no-margin-top ">
                    <div class="list no-hairlines-md no-margin-top margin-bottom-half">
                      <ul>
                        <li class="item-content item-input">
                          <div class="item-inner">
                            <div class="item-title item-label padding-bottom-half" style="font-size: 20px;">نام و نام خانوادگی</div>
                            <div class="item-input-wrap">
                              <input type="text" class="text-align-center name" placeholder="نام و نام خانوادگی مشتری را وارد کنید" style="border:1px solid rgb(247, 247, 247); border-radius: 4px;"/>
                              <span class="input-clear-button"></span>
                            </div>
                          </div>
                        </li>
                        <li class="item-content item-input">
                          <div class="item-inner">
                            <div class="item-title item-label padding-bottom-half" style="font-size: 20px;">شماره تلفن همراه</div>
                            <div class="item-input-wrap">
                              <input type="number" class="text-align-center mobile" placeholder="شماره تلفن مشتری را وارد کنید" style="border:1px solid rgb(247, 247, 247); border-radius: 4px;"/>
                              <span class="input-clear-button"></span>
                            </div>
                            
                          </div>
                        </li>
                        <li class="item-content item-input">
                          <div class="item-inner">
                            <div class="item-title item-label padding-bottom-half" style="font-size: 20px;">آدرس</div>
                            <div class="item-input-wrap">
                              <input type="text" class="text-align-center address" placeholder="آدرس  مشتری را وارد کنید" style="border:1px solid rgb(247, 247, 247); border-radius: 4px;"/>
                              <span class="input-clear-button"></span>
                            </div>
                          </div>
                        </li>
                        <li class="item-content item-input">
                          <div class="item-inner">
                            <div class="item-title item-label padding-bottom-half" style="font-size: 20px;">نوع مشتری</div>
                            <div class="item-input-wrap">
                              <div class="row padding-right-half">
                                <div class="col-40">
                                  <label class="radio"><input type="radio" name="demo-radio-inline" class="normal"/><i class="icon-radio"></i></label>
                                  همکار
                                </div>
                                <div class="col-50">
                                  <label class="radio"><input type="radio" name="demo-radio-inline" checked/><i class="icon-radio"></i></label>
                                  عادی
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="item-content item-input">
                          <div class="item-inner">
                            <div class="item-title item-label padding-bottom-half" style="font-size: 20px;">توضیحات</div>
                            <div class="item-input-wrap">
                              <textarea placeholder="توضیحات را وارد کنید" tyle="border:1px solid rgb(247, 247, 247); border-radius: 4px;" class="info"></textarea>
                              
                              <span class="input-clear-button" style="margin-top: 55px; margin-right: 270px;"></span>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
              `,
            on: {
              closed: function () {
                app.popup.destroy('')
              }
            }
          }
        )
        userInfoPage.open()
        $$('a.boxBuyCustomerPage').on('click',function(e){
          var name = $$('input.name').val();
          var number = $$('input.mobile').val();
          var address = $$('input.address').val();
          var isnormal = !$$('input.normal').prop('checked');
          var info = $$('textarea.info').val();
          var user = JSON.parse(localStorage.getItem('user'));
          let isFound = false
          if (number == ''){
            if (name == ''){name = 'بدون نام'}
            localStorage.setItem('currentUser',JSON.stringify({'number':number,'name':name,'address':address,'isnormal':isnormal,'info':info}))
          } else {
            for (let oneUser of user){
              if (oneUser['number'] == number ){
                isFound = true;
                localStorage.setItem('currentUser',JSON.stringify({'number':oneUser['number'],'name':oneUser['name'],'address':oneUser['address'],'isnormal':oneUser['isnormal'],'info':oneUser['info']}))
                break;
              }
            }
            
            if (isFound == false){
              var newUser = {'number':number,'name':name,'address':address,'isnormal':isnormal,'info':info}
              user.push(newUser)
              localStorage.setItem('user',JSON.stringify(user))
              localStorage.setItem('currentUser',JSON.stringify(newUser))
            }
          }
          openNewPage('boxBuyCustomer',function(page){
            userInfoPage.close()
            new boxBuyCustomerPage()  
          })
        });
        $$('i.backToProductPage').on('click',function(e){
          userInfoPage.close()
        })
      } else {
        
        var newUser = {'number':'','name':'بدون نام','address':'','isnormal':true,'info':''}
        
        localStorage.setItem('currentUser',JSON.stringify(newUser))
        openNewPage('boxBuyCustomer',function(page){
          new boxBuyCustomerPage()  
        })
      }
      
      
    } else {
      let notProductBuy = app.toast.create({ 
        text: 'محصولی انتخاب نشده است',
        position:'bottom',
        closeTimeout:'2000',
        destroyOnClose:true
      });
      notProductBuy.open();
    }
    
  });

  $$('i.closeProduct').on('click',function(e){
    closePage('product',function(page){
      function deleteLocalSet(){
        
        localStorage.setItem('currentBoxBuy',JSON.stringify(''))
        buyProduct = []
      }
      function listionThree(){
        $$('div.saveChanges').on('click',function(e){
          let boxBuy = JSON.parse(localStorage.getItem('boxbuy'))
          let currentBoxBuy = JSON.parse(localStorage.getItem('currentBoxBuy'))
          let k = 0;
          for (let oneOfProduct of products){
            let amountBuyProduct = 0;
            for(let oneOfBuyProduct of buyProduct){
              if (oneOfBuyProduct['id'] == oneOfProduct['id']){
                amountBuyProduct = oneOfBuyProduct['weight']
                break;
              }
            }
            let amountBoxBuyProduct= 0;
            for(let oneOfBoxBuyProduct of currentBoxBuy['product']){
              if (oneOfBoxBuyProduct['id'] == oneOfProduct['id']){
                amountBoxBuyProduct = oneOfBoxBuyProduct['weight']
                break;
              }
            }
            products[k]['amount'] = parseInt(products[k]['amount']) + parseInt(amountBoxBuyProduct) - parseInt(amountBuyProduct)
            k =k+1;
          }
          localStorage.setItem('product',JSON.stringify(products))
          currentBoxBuy['product'] = buyProduct;
          let i = 0;
          for (let oneboxbuy of boxBuy){
            if (oneboxbuy['id'] != currentBoxBuy['id']){i = i+1;continue} 
            boxBuy[i]['product'] = buyProduct
            
          }
          
          localStorage.setItem('boxbuy',JSON.stringify(boxBuy))
          saveChangeBoxBuy.close()
          deleteLocalSet()
        })
        $$('div.DsaveChanges').on('click',function(e){
          saveChangeBoxBuy.close()
          deleteLocalSet();
          
        })
      }
      if (JSON.parse(localStorage.getItem('currentBoxBuy')) != ''){
        var saveChangeBoxBuy = app.dialog.create({
          content:`
              
              <div class='text-align-center'>ایا مایل به ذخیره تغییرات هستید ؟ </div>
              <div class='row no-gap margin-top-half'>
                <div class='button button-fill col-45 bg-color-lightblue saveChanges'><b>بله </b></div>
                <div class='button button-fill col-45 bg-color-deeppurple DsaveChanges'><b>خیر </b></div>
              </div>
            
            `,
            on: {
              closed: function () {
                app.dialog.destroy()
              }
            }
        });
        saveChangeBoxBuy.open();
        var seller = JSON.parse(localStorage.getItem('currentSeller'))
        if (seller['darkmode']){
          $$('div.dialog').addClass('dark')
        }
        
        listionThree()
      } else {
        deleteLocalSet()
      }
      
    });
  })

  // get the sum of product and put it on the page
  function updateSum(){
    sum = 0;
    for (let oneBuyProduct of buyProduct){
      for (let oneProduct of products){
        if (oneProduct['id'] != oneBuyProduct['id']){continue}
        sum = sum + ((oneProduct['price'] * (100-parseInt(oneProduct['discount'])) / 100) * parseInt(oneBuyProduct['weight']));
      }
    }
    $$('p.sumPrice').text(`جمع کل : ${sum}`)
  };
  
  // show the product with option
  function showProduct(searchProduct){
    previousSearch = searchProduct;
    let products = JSON.parse(localStorage.getItem('product'))
    let allOFProduct = ''
    for (let oneProduct of products){
      if (oneProduct['name'].includes(searchProduct) && oneProduct['category'].includes(currentCategory)){
        allOFProduct = allOFProduct + `
        <li class="no-margin">
          <div class="card margin justify-content-flex-start">
            <div class="item-media no-margin justify-content-flex-start padding-right-half" >
              <img src="https://cdn.framework7.io/placeholder/sports-1024x1024-1.jpg" width="90" />
              <div class="item margin-right justify-content-flex-start" style="width: 100%;">
                <div class='row no-gap' style='width:100%'>
                  <div class='col-30'>  
                    <div class="item-title" style="font-size:20px; padding-right:3px;"> ${oneProduct['name']}  </div>
                
                    
                    `
        if (oneProduct['discount'] > 0 ){
          allOFProduct = allOFProduct + `
            <div class="item-after text-color-gray" style="text-decoration: line-through; font-size:12px;" >
              ${oneProduct['price']} ریال
            </div>`   
        }
        allOFProduct = allOFProduct + `
        <div class="item-after " style="font-size:16px">
          <b>
            ${oneProduct['price'] * (100-parseInt(oneProduct['discount'])) / 100} ریال
          </b>
        </div>     
        </div> `

        if (oneProduct['discount'] > 0 ){
          allOFProduct = allOFProduct + `
            <div class='starBackground col-70 text-align-center ' style=" font-size:16px;">
             <div class='padding-bottom padding-right'> <div class='padding-bottom padding-right margin-right' style='padding-top:16px;'>
              ${oneProduct['discount']}%
             </div></div>
            </div>
          `}
        
        let isbuy = false
        for (let oneBuyProduct of buyProduct){
          if (oneBuyProduct['id'] != oneProduct['id']){continue;}
          isbuy = true;
          break;
        }
        allOFProduct = allOFProduct + `
              </div>
              <div class="item-after " style="font-size:16px">
               تعداد باقی ماتده در انبار : ${oneProduct['amount']}
              </div>
              </div>
            </div>
            <div class="item-inner padding-right padding-left">
              <div class='row no-gap' style="width:100%">
                <div class='item-text col-20 text-align-center padding-vertical-half' style='border-radius:5px; border:solid 1px #097723; background-color:#097723; color:white;`
                if(isbuy == false){allOFProduct = allOFProduct + `visibility:hidden;`}
                allOFProduct = allOFProduct + ` font-size:18px;'>
                  <b>`
                if (isbuy){
                  for (let oneBuyProduct of buyProduct){
                    if (oneBuyProduct['id'] != oneProduct['id']){continue}
                    allOFProduct = allOFProduct + `${oneBuyProduct['weight']}`
                    break
                  }
                }  
                allOFProduct = allOFProduct +  `</b>
                </div>
                <div class="item-text col-65">
                  <div style='visibility:hidden; height:0px'>${oneProduct['id']}</div>
                  <a href="" class="button button-fill id${oneProduct['id']} addProductToBox bg-color-customOne margin-horizontal-half" style='font-size:20px;'><b>افزودن به سبد</b></a>
                </div>
                
                <div class="item-text col-15" `
                if (isbuy ==  false ) {allOFProduct = allOFProduct +`style=' visibility:hidden;'`}
                allOFProduct =  allOFProduct +` > <div class'no-padding' style='visibility:hidden; height:0px'>${oneProduct['id']}</div>
                  <a href="" class="button button-outline padding-half id${oneProduct['id']} color-white removeProductFromBox bg-color-red" style='font-size:18px;'>
                    <i class="bi bi-trash3-fill"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </li>`;
      }
    }
    $$('ul.listProduct').html(allOFProduct);
    updateSum();
    listenerOne();
  }
}

function boxBuyCustomerPage(){
  var products = JSON.parse(localStorage.getItem('product'))
  function showBuyProduct(){
    let allOFBuyProduct = ''
    for (let oneBuyProduct of buyProduct){
      for (let oneProduct of products){
        if (oneProduct['id'] == oneBuyProduct['id']){
          allOFBuyProduct = allOFBuyProduct + `
        <li class="no-margin">
          <div class="card margin justify-content-flex-start">
            <div class="item-media no-margin justify-content-flex-start" >
              <img src="https://cdn.framework7.io/placeholder/sports-1024x1024-1.jpg" width="90" />
              <div class="item margin-right justify-content-flex-start" style="width: 100%;">
                <div class='row no-gap' style='width:100%'>
                  <div class='col-30'>  
                    <div class="item-title" style="font-size:20px; padding-right:3px;"> ${oneProduct['name']}  </div>
                
                    
                    `
        if (oneProduct['discount'] > 0 ){
          allOFBuyProduct = allOFBuyProduct + `
            <div class="item-after text-color-gray" style="text-decoration: line-through; font-size:12px;" >
              ${oneProduct['price']} ریال
            </div>`   
        }
        allOFBuyProduct = allOFBuyProduct + `
        <div class="item-after " style="font-size:16px">
          <b>
            ${oneProduct['price'] * (100-parseInt(oneProduct['discount'])) / 100} ریال
          </b>
        </div>     
        </div> `

        if (oneProduct['discount'] > 0 ){
          allOFBuyProduct = allOFBuyProduct + `
            <div class='starBackground col-70 text-align-center ' style=" font-size:16px;">
             <div class='padding-bottom padding-right'> <div class='padding-bottom padding-right margin-right' style='padding-top:16px;'>
              ${oneProduct['discount']}%
             </div></div>
            </div>
          `}
        
        let isbuy = false
        for (let oneBuyProduct of buyProduct){
          if (oneBuyProduct['id'] != oneProduct['id']){continue;}
          isbuy = true;
          break;
        }
        allOFBuyProduct = allOFBuyProduct + `
              </div>
              <div class="item-after " style="font-size:16px">
               تعداد باقی ماتده در انبار : ${oneProduct['amount']}
              </div>
              </div>
            </div>
            <div class="item-inner padding-right padding-left">
              <div class='row no-gap' style="width:100%">
                <div class='item-text col-20 text-align-center padding-vertical-half' style='border-radius:5px; border:solid 1px #097723; color:#097723;`
                if(isbuy == false){allOFBuyProduct = allOFBuyProduct + `visibility:hidden;`}
                allOFBuyProduct = allOFBuyProduct + ` font-size:18px;'>
                  <b>`
                if (isbuy){
                  for (let oneBuyProduct of buyProduct){
                    if (oneBuyProduct['id'] != oneProduct['id']){continue}
                    allOFBuyProduct = allOFBuyProduct + `${oneBuyProduct['weight']}`
                    break
                  }
                }  
                allOFBuyProduct = allOFBuyProduct +  `</b>
                </div>
                <div class="item-text col-65">
                  <div style='visibility:hidden; height:0px'>${oneProduct['id']}</div>
                  <a href="" class="button button-fill id${oneProduct['id']} addProductToBox bg-color-customOne margin-horizontal-half" style='font-size:20px;'><b>افزودن به سبد</b></a>
                </div>
                
                <div class="item-text col-15" `
                if (isbuy ==  false ) {allOFBuyProduct = allOFBuyProduct +`style=' visibility:hidden;'`}
                allOFBuyProduct =  allOFBuyProduct +` > <div class'no-padding' style='visibility:hidden; height:0px'>${oneProduct['id']}</div>
                  <a href="" class="button button-outline padding-half id${oneProduct['id']} color-red removeProductFromBox" style=font-size:20px;>
                    <i class="bi bi-trash3-fill"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </li>`;
             
        };
      }
    }
    $$('ul.listBuyProduct').html(allOFBuyProduct);
    listenerTwo();
    updateSumBoxBuy()
  }
  function listenerTwo(){
    $$('a.addProductToBox').on('click',function(e){
      
      let addProduct = $$(this).parent().children('div').text();
      for (let oneProduct of products){
        if (oneProduct['id'] != addProduct){
          continue;
        }
        let dialogProductAmount = app.dialog.create({
          content:`
                  <div class='text-align-center justify-content-center no-margin no-padding' style="font-size: 14px;">
                    <h4>${oneProduct['modulity']} محصول خود را به ${oneProduct['vahed']} وارد کنید</h4>
                    <input type="number" name="name" placeholder= ${oneProduct['vahed']} class=' justify-content-center amountProduct' style='border:1px solid gray; border-radius:2px; padding:5px; width:100%'/>
                   
                  `,
          closeByBackdropClick:true,
          buttons:[
            {
              text:'تایید',
              bold:true,
              color:'white',
              cssClass:'margin-left padding-horizontal bg-color-blue',
              onClick:function(dialog,e){
                if($$('input.amountProduct').val()){
                  let oneProduct ;
                  for ( oneProduct of products){
                    if (oneProduct['id'] != addProduct ){continue}
                    break
                  }
                  if (parseInt(oneProduct['amount']) < parseInt($$('input.amountProduct').val())){
                    app.dialog.alert('محصول موردنظر به تعداد کافی وجود ندارد',' ',function(dialog,e){app.dialog.close();app.dialog.close});
                  } else {
                    if($$('input.amountProduct').val() == 0){
                      let i =0 ;
                      for (let oneProductBuy of buyProduct){
                        if (oneProductBuy['id'] == addProduct){
                          buyProduct.splice(i,1);
                        }
                      }

                    } else {
                      let isexist =false ;
                      for (let oneProductBuy of buyProduct){
                        if (oneProductBuy['id'] == addProduct){
                          oneProductBuy['weight'] = $$('input.amountProduct').val();
                          isexist = true;
                        }
                      }
                      if (isexist == false){
                        buyProduct.push({'id':oneProduct['id'],'weight':$$('input.amountProduct').val()})
                      }
                                       
                      localStorage.setItem('product',JSON.stringify(products))
                    }
                  }
                }
                showBuyProduct();
                dialog.close();
              }
            },
            {text:'انصراف',bold:true,color:'white',cssClass:'margin-horizontal padding-horizontal bg-color-red',onClick:function(dialog,e){dialog.close()}}
          ],
          on:{
            'closed':function(dialog){
            app.dialog.destroy();
            }
          }
        });
        dialogProductAmount.open();
        updateSumBoxBuy();
        
        break;
      }  
      
    });

    $$('a.removeProductFromBox').on('click',function(e){
      let removeProduct = $$(this).parent().children('div').text();
      let i =0 ;
      for (let oneProductBuy of buyProduct){
        if (oneProductBuy['id'] == removeProduct){
         
          for (let oneProduct of products){
            if(oneProduct['id'] != removeProduct){continue}
          }
          localStorage.setItem('product',JSON.stringify(products))
          buyProduct.splice(i,1);
          showBuyProduct();
          break;
        }
        i = i+1;
      }
      updateSumBoxBuy();
      
    });
  }
  function updateSumBoxBuy(){
    sum = 0;
    for (let oneBuyProduct of buyProduct){
      for (let oneProduct of products){
        if (oneProduct['id'] != oneBuyProduct['id']){continue}
        sum = sum + ((oneProduct['price'] * (100-parseInt(oneProduct['discount'])) / 100) * parseInt(oneBuyProduct['weight']));
      }
    }
    $$('p.sumPrice').text(`جمع کل : ${sum}`)
  };
  showBuyProduct()
  $$('a.payMoney').on('click',function(e){
    openNewPage('payment',function(page){
      new paymentPage()  ;
    });
  })
  $$('i.addToCarts').on('click',function(e){
    let currentBoxBuy = JSON.parse(localStorage.getItem('currentBoxBuy'))
    if (currentBoxBuy != ''){
      currentBoxBuy = currentBoxBuy['id']
    }
    let dialogBoxBuy = app.dialog.create({
      content:`
               <div class='text-align-center justify-content-center no-margin no-padding' style="font-size: 14px;">
               <h4>شماره سفارش وارد کنید</h4>
               <input class="boxBuyId" type="number" name="name" value='${currentBoxBuy}' class=' justify-content-center amountProduct' style='border:1px solid gray; border-radius:2px; padding:5px; width:100%'/>
     
              `,
      closeByBackdropClick:true,
      buttons:[
        {
          text:'تایید',
          bold:true,
          color:'white',
          cssClass:'margin-left padding-horizontal bg-color-blue',
          onClick:function(dialog,e){
            let time = new Date()
            
            let currentBoxBuy = JSON.parse(localStorage.getItem('currentBoxBuy'))
            let k = 0;
            for (let oneOfProduct of products){
              let amountBuyProduct = 0;
              for(let oneOfBuyProduct of buyProduct){
                if (oneOfBuyProduct['id'] == oneOfProduct['id']){
                  amountBuyProduct = oneOfBuyProduct['weight']
                  break;
                }
              }
              let amountBoxBuyProduct= 0;
              if (currentBoxBuy != ''){
                for(let oneOfBoxBuyProduct of currentBoxBuy['product']){
                  if (oneOfBoxBuyProduct['id'] == oneOfProduct['id']){
                    amountBoxBuyProduct = oneOfBoxBuyProduct['weight']
                    break;
                  }
                }
              }
              products[k]['amount'] = parseInt(products[k]['amount']) + parseInt(amountBoxBuyProduct) - parseInt(amountBuyProduct)
              k =k+1;
            }
            localStorage.setItem('product',JSON.stringify(products))
            var boxBuyId = $$('input.boxBuyId').val()
            if (boxBuyId){
              let boxbuy = JSON.parse(localStorage.getItem('boxbuy'));
              let isexist = false
              for (let onebuxbuy of boxbuy){
                if (onebuxbuy['id'] == boxBuyId){
                  onebuxbuy['product'] = buyProduct
                  isexist = true
                }
                
              }
              if (isexist == false){
                console.log(time.getHours() , time.getMinutes(),time.getSeconds())
                boxbuy.push({'id':boxBuyId,'time':`${time.getHours()}${time.getMinutes()}${time.getSeconds()}`,'seller':JSON.parse(localStorage.getItem('currentSeller')),'product':buyProduct,'user':JSON.parse(localStorage.getItem('currentUser'))})
              }
              if (currentBoxBuy != ''){
                if(currentBoxBuy['id'] != boxBuyId){
                  let i = 0
                  boxbuy = JSON.parse(localStorage.getItem('boxbuy'))
                  for (let oneboxbuy of boxbuy){
                    if(oneboxbuy['id'] == currentBoxBuy['id']){
                      boxbuy.splice(i,1)
                    }
                    i =i+1;
                  }
                  
                }
              }
              localStorage.setItem('boxbuy',JSON.stringify(boxbuy))
              backToPage('mainMenu',function(page){});
            }
          }
        },
        {text:'انصراف',bold:true,color:'white',cssClass:'margin-horizontal padding-horizontal bg-color-red',onClick:function(dialog,e){dialog.close()}}
      ],
      on:{
        'closed':function(){
          app.dialog.destroy();
        }
      }
    });
    dialogBoxBuy.open();
  })
  $$('i.backToUserInfoPage').on('click',function(e){
    closePage('boxBuyCustomerPage',function(page){
      
    });
  })
}

function paymentPage(){
  let paymnet = 0;
  var products = JSON.parse(localStorage.getItem('product'))
  
  function remainingPayment(){
    sum = 0;
    for (let oneBuyProduct of buyProduct){
      for (let oneProduct of products){
        if (oneProduct['id'] != oneBuyProduct['id']){continue}
        sum = sum + ((oneProduct['price'] * (100-parseInt(oneProduct['discount'])) / 100) * parseInt(oneBuyProduct['weight']));
      }
    }
    var withmaliat = JSON.parse(localStorage.getItem('currentSeller'))['maliat']

    if (withmaliat) {
      sum = sum + (0.09 * sum);
    }
    let cash = $$('input.cash').val();
    let sale = $$('input.sale').val();
    let additional = $$('input.additional').val();
    if (!cash){cash = 0};
    if (!sale){sale = 0};
    if (!additional){additional = 0};
    
    paymnet = parseInt(cash) + parseInt(sale) - parseInt(additional);
    $$('p.sumPrice').text(`مبلغ کل : ${sum}`)
    if ($$('p.sumPriceNow').hasClass('text-color-green')){
      $$('p.sumPriceNow').removeClass('text-color-green');
      $$('p.sumPriceNow').addClass('text-color-blue');
    }
    if (parseInt(sum)-parseInt(paymnet) <= 0 ){
      if ($$('p.sumPriceNow').hasClass('text-color-blue')){
        $$('p.sumPriceNow').removeClass('text-color-blue');
        $$('p.sumPriceNow').addClass('text-color-green');
      }
    }
    $$('p.sumPriceNow').text(` مبلغ قابل پرداخت : ${parseInt(sum) - parseInt(paymnet)}`)

   }
  
  remainingPayment();
  
  $$('input.cash').on('keyup',function(e){remainingPayment();})
  $$('input.sale').on('keyup',function(e){remainingPayment();})
  $$('input.additional').on('keyup',function(e){remainingPayment();})
  $$('i.backToBoxBuyCustomerPage').on('click',function(e){
    closePage('paymentpage',function(page){
      
    });
  })
}

function boxBuyPage(){
  let isSort = false;
  let lastSearch = ''
  showBoxBuy(isSort,'')
  function showBoxBuy(isSort,search){
    let boxbuy = JSON.parse(localStorage.getItem('boxbuy'))
    let allOfboxbuy = ''
    if (isSort){
      boxbuy.sort( (a,b) => a['id'] - b['id'] );
    }
    for (let oneboxbuy of boxbuy){
      if (!oneboxbuy['id'].includes(search)){
        continue
      }
      allOfboxbuy = allOfboxbuy + `
        <li class="margin-bottom-half col-90 text-color-white" id= "listitem${oneboxbuy['id']}" style="padding-left:4px; padding-right:4px;">`
      
      if (oneboxbuy['user']['isnormal']){
       allOfboxbuy = allOfboxbuy + `    <div class="card no-margin justify-content-flex-start listBoxBuyItem padding-half" style="margin-horizontal:1px; border-radius: 10px; background-image: linear-gradient(to bottom left, #00bbbb, #3399ff);">`
      } else {
        allOfboxbuy = allOfboxbuy + `<div class="card no-margin justify-content-flex-start listBoxBuyItem padding-half" style="margin-horizontal:1px; border-radius: 10px; background-image: linear-gradient(to bottom left, #D4AF37, #FFD700);">`
      }
      allOfboxbuy = allOfboxbuy +    
           `<div class="item-media no-margin justify-content-flex-start" >
              <div class='row no-gap' style='width:100%'>
                <div class='col-70'>
                  
                  <div class="item-title padding-right-half" style = 'font-size:18px;'> نام مشتری : ${oneboxbuy['user']['name']}</div>  
                  <div class="item-title padding-right-half" style = 'font-size:18px;'> تاریخ ثبت : ${(parseInt((oneboxbuy['time']%10000)/100))} : ${parseInt(oneboxbuy['time']/10000)} </div>  
                </div>
                <div class='col-20'>`
                if (parseInt(oneboxbuy['id']) > 99999){
                  allOfboxbuy = allOfboxbuy + `<div class="item-title text-align-center bg-color-white text-color-black padding-vertical" style='border-radius:30px;font-size:12px; '> ${oneboxbuy['id']}</div> `
                } 
                else if (parseInt(oneboxbuy['id']) > 999){
                  allOfboxbuy = allOfboxbuy + `<div class="item-title text-align-center bg-color-white text-color-black padding-vertical" style='border-radius:30px;font-size:18px; '> ${oneboxbuy['id']}</div> `
                } else {
                  allOfboxbuy = allOfboxbuy + `<div class="item-title text-align-center bg-color-white text-color-black padding-vertical-half" style='border-radius:30px;font-size:28px; '> ${oneboxbuy['id']}</div> `
                }
                allOfboxbuy = allOfboxbuy  + `</div>
                  
                
              </div>
            </div>  
          
        </li>`;
      }
    
    $$('ul.listBoxBuy').html(allOfboxbuy);
  }
  $$('div.listBoxBuyItem').on('click',function(e){
    let boxBuyID = $$(this).parent().attr('id').split('listitem')[1]
    let allbox = JSON.parse(localStorage.getItem('boxbuy'))
    let oneboxbuy
    for (oneboxbuy of allbox){
      if (oneboxbuy['id'] == boxBuyID){
        break
      }
      
    }
    localStorage.setItem('currentBoxBuy',JSON.stringify(oneboxbuy))
    buyProduct = oneboxbuy['product']
    openNewPage('product',function(page){
      new productPage();
    });
  })
  $$('i.closeBoxBuy').on('click',function(e){
    closePage('boxBuy',function(page){
      
    });
  })
  $$('i.sortById').on('click',function(e){
    if (isSort){
      isSort=false
    } else {
      isSort = true
    }
    showBoxBuy(isSort,lastSearch)
  })
  $$('input.searchBoxBuy').on('keyup',function(e){
    lastSearch = $$(this).val()
    showBoxBuy(isSort,lastSearch)
  })
}
var assert = require('assert');

function add() {
  return Array.prototype.slice.call(arguments).reduce(function(prev, curr) {
    return prev + curr;
  }, 0);
}

describe('add()', function() {
  var tests = [
    {args: [1, 2],       expected: 3},
    {args: [1, 2, 3],    expected: 6},
    {args: [1, 2, 3, 4], expected: 10}
  ];

  tests.forEach(function(test) {
    it('correctly adds ' + test.args.length + ' args', function() {
      var res = add.apply(null, test.args);
      assert.equal(res, test.expected);
    });
  });
});
