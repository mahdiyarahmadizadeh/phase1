export class Product{
  constructor(id,name,price,discount,category,modulity,weight,vahed){
    this.id = id;
    this.name = name;
    this.price = price;
    this.discount = discount;
    this.category = category;
    this.modulity = modulity;
    this.weight = weight;
    this.vahed = vahed;
  }
  getID(){
    return this.id;
  }
  getName(){
    return this.name;
  }
  getPrice(){
    return this.price;
  }
  getDiscount(){
    return this.discount;
  }
  getCategory(){
    return this.category;
  }
  getModulity(){
    return this.modulity;
  }
  getWeight(){
    return this.weight;
  }
  setWeight(newWeight){
    this.weight = newWeight
  }
  getVahed(){
    return this.vahed;
  }
}

